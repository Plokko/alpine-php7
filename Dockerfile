###########################################################
##			Alpine Apache + mod_php7
###########################################################
FROM alpine:latest

## Upgrade & install
RUN apk upgrade --update --no-cache && \
	apk add --no-cache \
		ca-certificates openssl curl mysql-client \
		php7 php7-opcache \
		php7-pdo php7-openssl php7-mbstring php7-mcrypt php7-tokenizer php7-ctype php7-fileinfo \
		php7-xml php7-xmlreader php7-json php7-bcmath php7-zlib php7-zip php7-bz2 php7-sqlite3 php7-iconv \
		php7-simplexml php7-xmlwriter php7-pdo_sqlite php7-soap php7-xmlrpc php7-xsl \
		php7-gd php7-curl php7-imagick php7-pdo_mysql php7-mysqli php7-phar php7-intl php7-imap php7-ldap php7-exif \
		php7-sockets php7-mysqlnd php7-mysqli php7-redis php7-memcached
		

# Copy default php configuration
COPY ./default-cfg/php.ini /etc/php7/php.ini

###################
## Remove apk cache
#RUN rm -R /var/cache/apk/*


# Run apache in foreground
CMD ["php","-v"]
